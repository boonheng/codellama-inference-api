import pytest
from async_asgi_testclient import TestClient
from fastapi import status

@pytest.mark.asyncio
async def test_helloworld(client: TestClient) -> None:

    resp = await client.get(
        "/api/v1/hello",
    )
    resp_json = resp.json()

    assert resp.status_code == status.HTTP_200_OK
    assert resp_json["text"] == "hello world"