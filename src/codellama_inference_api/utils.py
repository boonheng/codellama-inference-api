import logging
import string
import random

logger = logging.getLogger(__name__)
ALPHA_NUMERAL = string.ascii_letters + string.digits

def generate_random_id(length: int = 20) -> str:
    return "".join(random.choices(ALPHA_NUMERAL, k=length))