from typing import Any

from fastapi import APIRouter, Response, status


router = APIRouter()

@router.get("/hello")
async def hello_world() -> dict[str, str]:
    from ..config import settings
    return {
        "text": "hello world",
        "environment": settings.ENVIRONMENT
    }

@router.get("/helloworld")
def say_hello(name: str) -> str:
    return f"Hello {name}"
