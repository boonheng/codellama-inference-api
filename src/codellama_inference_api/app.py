from contextlib import asynccontextmanager
import asyncio
from typing import Any
from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from starlette.middleware.cors import CORSMiddleware

from .config import app_configs, settings
from .api.router import router as api_router

app = FastAPI(**app_configs)

app.add_middleware(
    CORSMiddleware,
    allow_origins = settings.CORS_ORIGINS,
    allow_origin_regex = settings.CORS_ORIGIN_REGEX,
    allow_credentials = True,
    allow_methods = ("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"),
    allow_headers = settings.CORS_HEADERS,
)

@app.get("/", include_in_schema=False)
async def root_redirect():
    response = RedirectResponse(url='/docs')
    return response

@app.get("/healthz", include_in_schema=False)
async def healthcheck() -> dict[str, str]:
    return {"status": "ok"}

app.include_router(api_router, prefix="/v1", tags=["api"])