from codellama_inference_api.app import app
from ray import serve
from fastapi import FastAPI

# app = FastAPI()

# @app.get("/")
# def f():
#     return "Hello from the root"

@serve.deployment(route_prefix="/")
@serve.ingress(app)
class FastAPIWrapper:
    pass

if __name__ == "__main__":
    # use this for debugging purposes only
    # import uvicorn
    # uvicorn.run(app, host="0.0.0.0", port=3000)
    serve.run(FastAPIWrapper.bind(), True, host="0.0.0.0", port=3000)
    import time
    while True:
      time.sleep(2000)